# -*- coding: utf-8 -*-
"""
Created on Fri Jan 08 09:46:14 2016

@author: User
"""
import numpy as np
#import filecmp

rave_1 = np.loadtxt("K2Campaign7-ObservedTargets_groundbased_masterlist_070116.txt")
#rave_2 = np.loadtxt("K2Campaign2-Observed_RAVE.txt")
web = np.loadtxt("K2Campaign7.txt")
#==============================================================================
#how to compare two sets of data files directly 
#ans = filecmp.cmp('K2Campaign1_ObservedTargets_groundbased_masterlist_070116.txt','K2Campaign1_Observed_RAVE.txt')
# print ans
#==============================================================================

EPIC_1 = rave_1[:,0]
GALAH_1 = rave_1[:,1]
APOGEE_1 = rave_1[:,2]
LAMOST_1 = rave_1[:,3]
Gaia_ESO_1 = rave_1[:,4]
SAGA_1 = rave_1[:,5]
RAVE_1 = rave_1[:,6]

#==============================================================================
# EPIC_2 = rave_2[:,0]
# GALAH_2 = rave_2[:,1]
# APOGEE_2 = rave_2[:,2]
# LAMOST_2 = rave_2[:,3]
# Gaia_ESO_2 = rave_2[:,4]
# SAGA_2 = rave_2[:,5]
# RAVE_2 = rave_2[:,6]
#==============================================================================

EPIC_3 = web[:,0]
GALAH_3 = web[:,1]
APOGEE_3 = web[:,2]
LAMOST_3 = web[:,3]
Gaia_ESO_3 = web[:,4]
SAGA_3 = web[:,5]

if len(EPIC_1)==len(EPIC_3):
    print 'Alles gut'

#==============================================================================
# EPIC = np.array_equal(EPIC_1,EPIC_2)
# print EPIC
# GALAH = np.array_equal(GALAH_1,GALAH_2)
# print GALAH
# APOGEE = np.array_equal(APOGEE_1,APOGEE_2)
# print APOGEE
# LAMOST = np.array_equal(LAMOST_1,LAMOST_2)
# print LAMOST
# Gaia_ESO = np.array_equal(Gaia_ESO_1,Gaia_ESO_2)
# print Gaia_ESO
# SAGA = np.array_equal(SAGA_1,SAGA_2)
# print SAGA
# RAVE = np.array_equal(RAVE_1,RAVE_2)
# print RAVE
#==============================================================================

epic = np.array_equal(EPIC_1,EPIC_3)
print epic
galah = np.array_equal(GALAH_1,GALAH_3)
print galah
apogee = np.array_equal(APOGEE_1,APOGEE_3)
print apogee
lamost = np.array_equal(LAMOST_1,LAMOST_3)
print lamost
Gaia_Eso = np.array_equal(Gaia_ESO_1,Gaia_ESO_3)
print Gaia_Eso
saga = np.array_equal(SAGA_1,SAGA_3)
print saga
